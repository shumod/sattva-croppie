/**
 * @file
 */

/**
 * Provides a farbtastic colorpicker for the fancier widget.
 */
(function ($) {
  Drupal.behaviors.field_croppie = {
    attach: function(context) {
      $(document).once('document-ready').ready(function(){
        var field_id = $('.field-widget-croppie-image').attr('id');

        if(
          !$('.croppie-field-wrapper').parent().find('input[type=submit][value=Remove]').length ||
          $('#'+field_id+'-und-hidden-image').val()
        ){
          $('.croppie-field-wrapper').removeClass('element-hidden');
        }
        else{
          $('.croppie-field-wrapper').addClass('element-hidden');
        }

        if($('#'+field_id+'-und-hidden-image').val()){
          initCrop($('#'+field_id+'-und-hidden-image').val(), field_id);
        }

        $('#'+field_id+'-und-file').once('bind-field_image').bind('change', function () {
          initCrop(null, field_id);
          readFile(this);
        });
      });


      function initCrop(url, field_id){
        $('.upload-demo-wrap').removeClass('element-hidden');
        $('#sattva-image-remove').removeClass('element-hidden');

        var settings = {
          viewport: {
            width: 200,
            height: 200,
            type: 'square'
          },
          enableZoom: true,
          enableExif: true
        };

        if(url){
          settings.url = url;
        }

        if(!Drupal.settings.uploadCrop) {
          Drupal.settings.uploadCrop =
            $('#upload-demo')
              .once('croppie-init')
              .croppie(settings)
              .bind('update.croppie', function (ev, cropData) {
                $this = $(this);
                $this.croppie('result', {
                  type: 'canvas',
                  size: 'viewport',

                }).then(function (resp) {
                  $('#'+field_id+'-und-hidden-image').val(resp);
                  $('#sattva-image-remove')
                    .once('sattva-image-remove')
                    .removeClass('element-hidden')
                    .bind('click', function(e){
                      e.preventDefault();
                      $('.upload-demo-wrap').addClass('element-hidden');
                      $(this).addClass('element-hidden');
                      $('#'+field_id+'-und-hidden-image').val('');
                      $('#'+field_id+'-und-file').val('');
                    });
                });
              });

          setTimeout(function(){
            Drupal.settings.uploadCrop.croppie('setZoom', 0)
          }, 100);

          console.log(Drupal.settings.uploadCrop);
        }
      }

      function readFile(input) {
        if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
            $('.upload-demo').addClass('ready');
            Drupal.settings.uploadCrop.croppie('bind', {
              url: e.target.result
            }).then(function(){
              console.log('jQuery bind complete');
              setTimeout(function(){
                Drupal.settings.uploadCrop.croppie('setZoom', 0)
              }, 100);
            });
          }

          reader.readAsDataURL(input.files[0]);
        }
        else {
          swal("Sorry - you're browser doesn't support the FileReader API");
        }
      }
    }
  }
})(jQuery);
